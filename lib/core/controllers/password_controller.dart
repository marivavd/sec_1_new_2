import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';


class PasswordController extends TextEditingController{
  String get_hash_text(){
    var bytes = utf8.encode(text);
    return sha256.convert(bytes).toString();
  }
}