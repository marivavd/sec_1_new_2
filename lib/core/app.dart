import 'package:flutter/material.dart';
import 'package:sec_1_new_2/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new_2/core/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: light_theme,
      darkTheme: dark_theme,
      home: Sign_in_Page(),
    );
  }
}