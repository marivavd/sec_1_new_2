import 'package:flutter/material.dart';
import 'package:sec_1_new_2/auth/data/repository/supabase.dart';
import 'package:sec_1_new_2/auth/domain/itils.dart';
import 'package:sec_1_new_2/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 46,
          width: 342,
          child: FilledButton(
            style: Theme.of(context).filledButtonTheme.style,
            onPressed: ()async{
              checkInternet(context, (){
                sign_out(context: context,
                    onError: (String e){showError(context, e);},
                    onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => Sign_in_Page()));});
              });
            },
            child: Text(
              "ВЫХОД",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w700
              ),
            ),
          ),
        ),
      ),
    );
  }
}
