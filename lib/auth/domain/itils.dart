import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';

Future<void> checkInternet(BuildContext context, Function onGood)async{
  var connectivity = await Connectivity().checkConnectivity();
  if (connectivity == ConnectivityResult.none){
    showError(context, "No Internet connection");
  }
  else{
    onGood();
  }
}