import 'package:flutter/material.dart';
import 'package:sec_1_new_2/auth/data/repository/supabase.dart';
import 'package:sec_1_new_2/auth/domain/itils.dart';
import 'package:sec_1_new_2/auth/presentation/pages/otp.dart';
import 'package:sec_1_new_2/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new_2/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new_2/core/controllers/password_controller.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';

class Forgot_pass extends StatefulWidget {
  const Forgot_pass({super.key});
  @override
  State<Forgot_pass> createState() => _Forgot_passState();
}

class _Forgot_passState extends State<Forgot_pass> {
  var email_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Восстановление пароля",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Введите свою почту"
              ),
              SizedBox(height: 28,),
              Custom_Field(label: "Почта", hint: "***********@mail.com", controller: email_controller),

              SizedBox(height: 503,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: ()async{
                      checkInternet(context, (){
                          send_email(
                              email: email_controller.text,
                              context: context,
                              onError: (String e){showError(context, e);},
                              onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => OTP(email: email_controller.text)));});
                      });
                    },
                    child: Text(
                      "Отправить код",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Я вспомнил свой пароль!",
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Вернуться",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              ),
              SizedBox(height: 32,)
            ],
          ),
        ),
      ),
    );
  }
}
