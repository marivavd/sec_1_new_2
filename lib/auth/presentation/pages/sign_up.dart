import 'package:flutter/material.dart';
import 'package:sec_1_new_2/auth/data/repository/supabase.dart';
import 'package:sec_1_new_2/auth/domain/itils.dart';
import 'package:sec_1_new_2/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new_2/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new_2/core/controllers/password_controller.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';

class Sign_up_Page extends StatefulWidget {
  const Sign_up_Page({super.key});
  @override
  State<Sign_up_Page> createState() => _Sign_up_PageState();
}

class _Sign_up_PageState extends State<Sign_up_Page> {
  var email_controller = TextEditingController();
  var password_controller = PasswordController();
  var confirm_password_controller = PasswordController();
  var password_obscure = true;
  var confirm_password_obscure = true;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Создать аккаунт",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Завершите регистрацию чтобы начать"
              ),
              SizedBox(height: 28,),
              Custom_Field(label: "Почта", hint: "***********@mail.com", controller: email_controller),
              SizedBox(height: 24,),
              Custom_Field(label: "Пароль", hint: "**********", controller: password_controller, is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
              SizedBox(height: 24,),
              Custom_Field(label: "Повторите пароль", hint: "**********", controller: confirm_password_controller, is_obscure: confirm_password_obscure, tap_suffix: (){
                setState(() {
                  confirm_password_obscure = !confirm_password_obscure;
                });
              },),
              SizedBox(height: 417,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: ()async{
                      checkInternet(context, (){
                        if (password_controller.get_hash_text() == confirm_password_controller.get_hash_text()){
                          sign_up(
                              email: email_controller.text,
                              password: password_controller.get_hash_text(),
                              context: context,
                              onError: (String e){showError(context, e);},
                              onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => Sign_in_Page()));});
                        }
                        else{
                          showError(context, "Passwords do not match");
                        }
                      });
                    },
                    child: Text(
                      "Зарегистрироваться",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w700
                    ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "У меня уже есть аккаунт! ",
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Войти",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              ),
              SizedBox(height: 32,)
            ],
          ),
        ),
      ),
    );
  }
}
