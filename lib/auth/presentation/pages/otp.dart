import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:sec_1_new_2/auth/data/repository/supabase.dart';
import 'package:sec_1_new_2/auth/domain/itils.dart';
import 'package:sec_1_new_2/auth/presentation/pages/new_pass.dart';
import 'package:sec_1_new_2/auth/presentation/pages/sign_in.dart';
import 'package:sec_1_new_2/auth/presentation/widgets/text_field.dart';
import 'package:sec_1_new_2/core/controllers/password_controller.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';

class OTP extends StatefulWidget {
  final String email;
  const OTP({super.key, required this.email});
  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  var controller = TextEditingController();
  var is_error = false;

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double separator = widthScreen / 6 - 32;


    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Верификация",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Введите 6-ти значный код из письма "
              ),
              SizedBox(height: 28,),
              Pinput(
                controller: controller,
                separatorBuilder: (context) => SizedBox(width: separator,),
                length: 6,
                defaultPinTheme: PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF5C636A))
                    )
                ),
                focusedPinTheme: PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF7576D6))
                    )
                ),
                submittedPinTheme: (!is_error) ? PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF7576D6)),
                        borderRadius: BorderRadius.zero
                    )
                ): PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFED3A3A)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                onChanged: (text){
                  setState(() {
                    is_error = false;
                  });
                },


              ),
              SizedBox(height: 48,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: ()async{
                      checkInternet(context, (){
                        send_email(
                            email: widget.email,
                            context: context,
                            onError: (String e){showError(context, e);},
                            onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => OTP(email: widget.email)));});
                      });
                    },
                    child: Text(
                      "Получить новый код",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF7576D6)),
                    ),
                  )
                ],
              ),

              SizedBox(height: 503,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: ()async{
                      checkInternet(context, (){
                        verify_otp(
                            email: widget.email,
                            code: controller.text,
                            context: context,
                            onError: (String e){showError(context, e);},
                            onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => New_pass()));});
                      });
                    },
                    child: Text(
                      "Сбросить пароль",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Я вспомнил свой пароль!",
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Sign_in_Page()));
                    },
                    child: Text(
                      "Вернуться",
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              ),
              SizedBox(height: 32,)
            ],
          ),
        ),
      ),
    );
  }
}
