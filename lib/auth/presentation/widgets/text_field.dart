import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Custom_Field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool is_obscure;
  final Function()? tap_suffix;
  final MaskTextInputFormatter? formatter;

  const Custom_Field({super.key, required this.label, required this.hint, required this.controller, this.is_obscure = false, this.tap_suffix, this.formatter});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        SizedBox(height: 8,),
        TextField(
          controller: controller,
          obscureText: is_obscure,
          obscuringCharacter: '*',
          inputFormatters: (formatter != null)?[formatter!]:[],
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFFCFCFCF)),
            suffix: (tap_suffix != null)?GestureDetector(
              onTap: tap_suffix,
              child: Image.asset("assets/eye-slash.png"),
            ): null,
            contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                width: 1,
                color: Color(0xFF5C636A)
              )
            )
          ),
        )
      ],
    );
  }

}