import 'package:flutter/material.dart';
import 'package:sec_1_new_2/core/run.dart';
import 'package:sec_1_new_2/core/widgets/dialogs.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> sign_up({
  required String email,
required String password,
  required BuildContext context,
required Function(String) onError,
required Function() onResponse
}) async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.signUp(
      email: email,
      password: password,
    );

    onResponse();
  }
  on AuthException catch (e){
    Navigator.of(context).pop();
    showError(context, e.toString());
  }
  on Exception catch (e){
    Navigator.of(context).pop();
    showError(context, "Something went wrong");
  }

}
Future<void> sign_in({
  required String email,
  required String password,
  required BuildContext context,
  required Function(String) onError,
  required Function() onResponse
}) async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.signInWithPassword(
      email: email,
      password: password,
    );

    onResponse();
  }
  on AuthException catch (e){
    Navigator.of(context).pop();
    showError(context, e.toString());
  }
  on Exception catch (e){
    Navigator.of(context).pop();
    showError(context, "Something went wrong");
  }

}

Future<void> sign_out({
  required BuildContext context,
  required Function(String) onError,
  required Function() onResponse
}) async{
  try{
    showLoading(context);
    await supabase.auth.signOut();

    onResponse();
  }
  on AuthException catch (e){
    showError(context, e.toString());
  }
  on Exception catch (e){
    showError(context, "Something went wrong");
  }

}

Future<void> send_email({
  required String email,
  required BuildContext context,
  required Function(String) onError,
  required Function() onResponse
}) async{
  try{
    showLoading(context);
    await supabase.auth.resetPasswordForEmail(email);

    onResponse();
  }
  on AuthException catch (e){
    showError(context, e.toString());
  }
  on Exception catch (e){
    showError(context, "Something went wrong");
  }

}
Future<void> verify_otp({
  required String email,
  required String code,
  required BuildContext context,
  required Function(String) onError,
  required Function() onResponse
}) async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email,
    );
    onResponse();
  }
  on AuthException catch (e){
    showError(context, e.toString());
  }
  on Exception catch (e){
    showError(context, "Something went wrong");
  }

}

Future<void> change_password({
  required String password,
  required BuildContext context,
  required Function(String) onError,
  required Function() onResponse
}) async{
  try{
    showLoading(context);
    final UserResponse res = await supabase.auth.updateUser(
      UserAttributes(
        password: password,
      ),
    );

    onResponse();
  }
  on AuthException catch (e){
    showError(context, e.toString());
  }
  on Exception catch (e){
    showError(context, "Something went wrong");
  }

}